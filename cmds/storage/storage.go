package main

import (
	"fmt"
	"os"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/storage"
)

const (
	STORAGE_OPT_NAME    = "b2c.storage"
	FILESYSTEM_OPT_NAME = "b2c.filesystem"

	STORAGE_OUTPUT   = "/etc/containers/storage.conf"
	STORAGE_TEMPLATE = "/etc/containers/storage.conf.tmpl"
)

func main() {
	storageOpt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: STORAGE_OPT_NAME})
	fsOpt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: FILESYSTEM_OPT_NAME})
	fses := filesystem.ListCmdlineFilesystems(fsOpt)

	fmt.Printf("\n# Parsing the cmdline options\n")
	cfg := storage.ParseCmdline(storageOpt, fses)

	fmt.Printf("\n# Reading the template\n")
	tmplRaw, err := os.ReadFile(STORAGE_TEMPLATE)
	if err != nil {
		fmt.Printf("ERROR: Failed to read the template: %s\n", err.Error())
		os.Exit(1)
	}

	out, err := os.Create(STORAGE_OUTPUT)
	if err != nil {
		fmt.Printf("ERROR: Failed to create the output configuration file: %s\n", err.Error())
		os.Exit(1)
	}

	err = cfg.Generate(string(tmplRaw), out)
	if err != nil {
		fmt.Printf("ERROR: Failed to generate the output configuration: %s\n", err.Error())
		os.Exit(1)
	}
	os.Exit(0)
}
