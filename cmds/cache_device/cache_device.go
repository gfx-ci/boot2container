package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cache_device"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/swap"
)

const (
	STORAGE_MOUNTPOINT        = "/storage"
	STORAGE_MOUNTPOINT_LEGACY = "/container"
	STORAGE_SWAPFILE          = STORAGE_MOUNTPOINT + "/swapfile"
	STORAGE_CACHE_TMP         = STORAGE_MOUNTPOINT + "/tmp"

	CACHE_DEVICE_OPT_NAME = "b2c.cache_device"
	FILESYSTEM_OPT_NAME   = "b2c.filesystem"
	SWAP_OPT_NAME         = "b2c.swap"

	ONE_MiB = 1024.0 * 1024.0
)

func to_MiB(size int64) float32 {
	return float32(size / ONE_MiB)
}

func mountSwap(swapCfg swap.SwapConfig) bool {
	success := false

	if swapCfg.Method == swap.MethodNone {
		fmt.Printf("\n# No swap was mounted\n\n")
		return success
	}

	// Log that we are about to mount the swap partition
	var extra string
	if swapCfg.Method == swap.MethodSwapFile {
		extra = fmt.Sprintf(" - Size %.2f MiB", to_MiB(swapCfg.Size))
	}
	fmt.Printf("\n# Mounting %s as a swap space%s\n\n", swapCfg.SwapTarget, extra)

	// Mount the swap space
	status := "DONE"
	if err := swapCfg.Swapon(); err != nil {
		fmt.Printf("ERROR: %s\n=> Disabling swap!", err.Error())
		status = "FAILED"
		success = true
	}
	fmt.Printf("\n# Mounting the swap: %s\n\n", status)

	return success
}

func mountCacheDevice() {
	// Start by creating the mountpoints, and linking
	if err := os.MkdirAll(STORAGE_MOUNTPOINT, 0755); err != nil {
		log.Panicf("Failed to create the '%s' mountpoint: %s\n", STORAGE_MOUNTPOINT, err.Error())
	}
	if err := os.Symlink(STORAGE_MOUNTPOINT, STORAGE_MOUNTPOINT_LEGACY); err != nil {
		log.Panicf("Failed to create the '%s' legacy mountpoint: %s\n", STORAGE_MOUNTPOINT_LEGACY, err.Error())
	}

	// Parse the wanted configuration
	cd_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: CACHE_DEVICE_OPT_NAME})
	fs_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: FILESYSTEM_OPT_NAME})
	fses := filesystem.ListCmdlineFilesystems(fs_opt)
	cfg := cache_device.ParseCmdline(cd_opt, fses)

	// Parse the swap partition configuration
	swap_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: SWAP_OPT_NAME})
	swapCfg := swap.ParseCmdline(swap_opt, STORAGE_SWAPFILE)

	// Resume from hibernation, if asked by the user using b2c.swap=...,resume and if the image is valid
	if err := swapCfg.AttemptResumeIfApplicable(); err != nil {
		fmt.Printf("\nResuming error: %s\n", err)
	}

	// Mount swap spaces backed by a block device
	if swapCfg.Method == swap.MethodBlockDevice {
		mountSwap(swapCfg)
	}

	if cfg.Mount(STORAGE_MOUNTPOINT) {
		// Reset the temporary cache directory
		fmt.Printf("\n# Clearing the temporary cache directory\n")
		os.RemoveAll(STORAGE_CACHE_TMP)
		os.MkdirAll(STORAGE_CACHE_TMP, 0750)

		// Mount the swap device
		if swapCfg.Method == swap.MethodSwapFile {
			// Remove any existing swap file ahead of computing the available space
			os.Remove(STORAGE_SWAPFILE)

			// Clamp the size of the swap file to 25% of the free space on the cache device
			if used, total, err := cache_device.GetDiskUsage(STORAGE_MOUNTPOINT); err == nil {
				clampedSize := min(swapCfg.Size, (total-used)/4)
				if clampedSize != swapCfg.Size {
					fmt.Printf("WARNING: Clamping the swapfile size to 25% of the cache device's free space")
					swapCfg.Size = clampedSize
				}
			}

			mountSwap(swapCfg)
		}

		// Show how much space is left on the drive
		if used, total, err := cache_device.GetDiskUsage(STORAGE_MOUNTPOINT); err == nil {
			fmt.Printf("\n# Cache device usage: %.2f / %.2f MiB (%.2f%%)\n", to_MiB(used), to_MiB(total),
				float32(used*100.0/total))
		}
	}
}

func unmountCacheDevice() {
	fmt.Printf("\n# Unmounting the swap space\n\n")
	swap_opt, _ := cmdline.FindOption(cmdline.OptionQuery{Name: SWAP_OPT_NAME})
	swapCfg := swap.ParseCmdline(swap_opt, STORAGE_SWAPFILE)
	err := swapCfg.Swapoff()
	if err != nil {
		fmt.Printf("\nERROR: %s\n", err.Error())
	}

	fmt.Printf("\n# Unmounting the cache device\n\n")
	err = filesystem.Unmount(STORAGE_MOUNTPOINT)
	if err != nil {
		fmt.Printf("\nERROR: %s\n", err.Error())
	}
}

func help() {
	fmt.Printf("Usage: %s [mount|unmount]", os.Args[0])

	os.Exit(1)
}

func main() {
	if len(os.Args) != 2 {
		help()
	}
	switch os.Args[1] {
	case "mount":
		mountCacheDevice()
	case "unmount":
		unmountCacheDevice()
	default:
		help()
	}
}
