build_container="no"

push_image() {
	[ -n "$CI_JOB_TOKEN" ] && [ -n "$CI_REGISTRY" ] && podman login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
	extra_podman_args=
	[[ $IMAGE_NAME =~ ^localhost.* ]] && extra_podman_args='--tls-verify=false'

	cmd="podman push --compression-format zstd $extra_podman_args $IMAGE_NAME"

	$cmd || true
	sleep 2
	$cmd
}

skopeo_inspect() {
	skopeomsg=`skopeo --override-variant $CONTAINER_VARIANT inspect --tls-verify=false docker://$IMAGE_NAME  2>&1 || true`

	if [[ $skopeomsg == *"manifest unknown"* ]]; then
		build_container="yes"
	elif [[ $skopeomsg == *"Digest"* ]] &&  [[ $skopeomsg == *"Layers"* ]]; then
		build_container="no"
	else
		build_container="network_problem"
	fi
}

check_if_build_needed_or_crash_on_network_error() {
	skopeo_inspect
	if [[ $build_container == "network_problem" ]]; then
		echo "There was a network problem, second try in 5 seconds..."
		sleep 5
		skopeo_inspect
		if [[ $build_container == "network_problem" ]]; then
			echo "The network problem is still present, third and last try in 10 seconds..."
			sleep 10
			skopeo_inspect
		fi
	fi

	if [[ $build_container == "network_problem" ]]; then
		# Fail the job, we don't know for sure whether the image is missing
		exit 1
	elif [[ $build_container == "yes" ]]; then
		return 0
	else
		return 1
	fi
}

# This function requires a "build()" function to be defined by the caller ahead of calling this function
build_and_push_container() {
	if check_if_build_needed_or_crash_on_network_error; then
		build

		# Push it to the container registry
		if [ -n "$IMAGE_NAME" ]; then
			push_image
		fi
	fi
}
