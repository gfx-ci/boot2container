#!/bin/bash
set -ex

. .gitlab-ci/build_functions.sh

    case $GOARCH in
        amd64)
            CONTAINER_VARIANT="linux/amd64"
            ;;
        arm64)
            CONTAINER_VARIANT='linux/arm64/v8'
            ;;
        arm)
            CONTAINER_VARIANT="linux/arm/v6"
            GOARM=6
            ;;
        riscv64)
            CONTAINER_VARIANT="linux/riscv64"
            ALPINE_VERSION=edge  # TODO: Remove this when riscv support lands in a release
            ;;
        *)
            echo "Unsupported platform!"
            exit 1
            ;;
    esac

build() {
    case $GOARCH in
        arm64)
            dnf install -y qemu-user-static-aarch64
            ;;
        arm)
            dnf install -y qemu-user-static-arm
            ;;
        riscv64)
            dnf install -y qemu-user-static-riscv
            ;;
    esac

    buildah build -f Dockerfile -t $IMAGE_NAME --build-arg ALPINE_VERSION=$ALPINE_VERSION --build-arg GOARCH=$GOARCH --build-arg GOARM=${GOARM:-} --platform $CONTAINER_VARIANT --net host -v `pwd`:/app .
}

build_and_push_container
