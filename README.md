# Boot2Container

<img src="/logo/logo128px_with_padding.png" alt="Logo of the project" align="left" style="margin-right: 20px"/>

**__WARNING__**: The interface is not stable just yet, and using this initramfs
may wipe your drive (not by default though). Use with caution!

Shipping containers have revolutionized the goods industry by standardizing the
way goods are packaged down to the their physical dimensions, load capacity, ...
which made it easy to stack them up on big container ships then offload them to
trains and trucks, and so long and so forth.

Just like physical containers, IT containers have brought this level of
standardization to allow reproducing a work/testing environment anywhere, without
affecting the running host. This explains why containers are now so ubiquitous,
and why they are the basis of the vast majority of automated test systems found
on Github, Gitlab, and other forges!

This projects aims to turn create a generic initramfs that initializes the HW
just enough to download and run containers (docker, OCI). This enables running on
your bare-metal hardware the same containers you may already run in your
development machine or in the cloud, thus simplifying your deployment!

## Features

 * Small size: Under 20MB, with a goal of achieving sub-10MB
 * Fast boot: Under 10s for the first boot to docker's hello-world, 5s on later boots
 * Simple: no daemons, under 1kLOC of code, easy to generate
 * Maintainable: All the heavy lifting done by Red Hat's [podman](https://podman.io/)

## Quick start

Download the latest initramfs/kernel combo for your architecture from our
[release page](https://gitlab.freedesktop.org/gfx-ci/boot2container/-/releases),
and name them initrd and kernel. Releases up to v0.9.10 can be found in our
[previous repo's release page](https://gitlab.freedesktop.org/mupuf/boot2container/-/releases).

For an x86_64 platform, you may then run it using this command line:

    $ qemu-system-x86_64 -kernel bzImage-x86_64 -initrd initramfs.linux_amd64.cpio.xz -nographic -m 384M -enable-kvm -append 'console=ttyS0 b2c.run="-ti docker.io/library/alpine:latest"'

After less than 10 seconds, you should be greeted with an Alpine Linux prompt,
running inside QEmu \o/.

To further experiment with the initramfs, try editing the kernel command line
by setting more `b2c.` options which are documented the following section!

## Options

The initramfs reads its parameters from the kernel command line. The only
required argument there is `b2c.run` which is the address to the container
that needs to be executed. As an [ipxe](https://ipxe.org/) script for an
x86_64 EFI platform, it would look like this:

    #!ipxe
    kernel /linux-amd64 initrd=b2c b2c.run='-ti docker.io/library/alpine:latest`
    initrd --name b2c /initramfs_amd64.cpio.xz
    boot

Here is the list of available options:

 * **Environment**:
   * **b2c.extra_args_url**: Source additional parameters from an HTTP/HTTPS URL.
     This can be useful to set up confidential volumes without leaking minio
     credentials or fscrypt keys to the containers via the kernel command line.
     Format: Same one as the kernel command line.
     WARNING: The `b2c.cache_device`, `b2c.swap`, `b2c.nbd`, `b2c.ntp_peer`,
     `b2c.login`, `b2c.httpd`, and `b2c.extra_args_url` parameters cannot be set
     via the extra argument url.
   * **b2c.hostname**: Set the hostname of the machine. Syntax: `b2c.hostname=myhostname`.
    Default: `boot2container`.
   * **b2c.keymap**: Set the keyboard layout of the machine. Supported keymaps are located in `config/keymaps/`. Examples:
     * `b2c.keymap=fi`
     * `b2c.keymap=fr-latin9`
 * **Network**:
    * **ip**: By default, boot2container will issue a DHCP request at boot on all
    wired network interfaces until it gets an IP from a DHCP server. This
    behaviour can be disabled by setting `ip=...` in the kernel cmdline which will
    allow you to either set a static IP, use DHCP early (`ip=dhcp`), or disable
    network support (`ip=off` or `ip=none`). For more information, check out
    [Linux's nfsroot.txt](https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt).
    WARNINGS:
      * Setting the NTP peer using `ip=` is unsupported. Use `b2c.ntp_peer` instead.
      * Only one interface can be configured, for multiple interfaces configuration,
      use `b2c.iface` instead.
    * **b2c.iface**: This option allows specifying the network configuration of a
      network interface. Unlike `ip=`, it is applied after the kernel has booted,
      but enables to set the network configuration of multiple interfaces. Like
      `ip=`, this option disables the default DHCP request issued by
      boot2container at boot.
      Example: `b2c.iface=eth0,address=192.168.42.10/24,gateway=192.168.42.254,nameserver=1.1.1.1`.
      Can take the following comma-separated options:
      * **dhcp** or **auto**: Enable DHCP on the target interface.
      * **address**: Use a static IP with a
        [CIDR format](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing),
        potentially overriding the value set by the DHCP client (`dhcp` or `auto`).
        Syntax: `address=<ip address>`.
      * **gateway**: Set the default network gateway, or override the value set by
        the DHCP client (`dhcp` or `auto`). Syntax: `gateway=<ip address>`
      * **route**: Add routes, or replace routes added by the DHCP client (`dhcp`
        or `auto`) . This option can be repeated multiple times.
        Syntax: `route=<destination network ip address>:<gateway ip address>`
      * **nameserver**: Add a domain name server. This option can be repeated multiple
        times. If `nameserver` is called for multiple interfaces, the last values will
        be registered with higher priority than the previous ones. Moreover, the values
        given by the DHCP client (*i.e.* `b2c.iface` with `dhcp` or `auto`, `ip=` kernel
        cmdline) will be registered with lower priority than names servers given from
        `b2c.iface`. Syntax: `nameserver=<dns ip address>`
        Example: `b2c.iface=eth0,auto,nameserver=8.8.8.8 b2c.iface=eth1,address=192.168.0.1/24,nameserver=9.9.9.9,nameserver=1.1.1.1` will use the following name servers list with higher first : 9.9.9.9, 1.1.1.1,
        8.8.8.8 and finally values given by DHCP on eth0.
      * **forward**: Enable ip forwarding (NOTE: this is a global option, not
        per-interface).
 * **Containers**:
   * **b2c.login**: Log in to private container registries.
     Example: `b2c.login=registry.freedesktop.org,username=mupuf,password=helloworld,[tls-verify=bool]`
   * **b2c.run**: [`podman run`](https://docs.podman.io/en/latest/markdown/podman-run.1.html)
     command line to be executed once the boot sequence is over. If set multiple times,
     the containers will be executed one after the other, in the same order as they are
     specified, regardless of their exit code (unless **b2c.pipefail** is set). Examples:
     * `b2c.run=docker://docker.io/library/hello-world`
     * `b2c.run="-ti docker://docker.io/library/alpine:latest /bin/sh"`
   * **b2c.run_post**: [`podman run`](https://docs.podman.io/en/latest/markdown/podman-run.1.html)
     command line to be executed *after* all the `b2c.run`, no matter how the previous
     containers exited. If set multiple times, the post containers will be executed one
     after the other, regardless of their exit code.
   * **b2c.run_service**: [`podman run`](https://docs.podman.io/en/latest/markdown/podman-run.1.html)
     command line to be executed in the background, before any of the `b2c.run` and
     `b2c.run_post` containers. The service will get shut down at the end of
     the container pipeline. This parameter can be repeated multiple times to start
     multiple services. Example:
     * `b2c.run_service="--privileged --pid=host docker://telegraf:latest"`
   * **b2c.pipefail**: Stop executing `b2c.run` containers as soon as one returns a
     non-zero exit code. This only affects `b2c.run`, not `b2c.run_post` nor `b2c.run_service`.
   * **b2c.storage**: Configure the container storage using a list of `key=val` options.
     Available options:
     * **driver**: Default Storage Driver. Default: `overlay`
     * **transient_store**: Set to true to make all container metadata saved in temporary storage. Default: `false`.
     * **additionalimagestores**: A pipe-separated list of filesystems that will be mounted and used as read-only image stores.
     * **pull_options**: Specify how storage is populated when pulling images as pipe-separated list. Supported options are: `enable_partial_images`, `use_hard_links`, and `convert_images`. Default: `enable_partial_images|use_hard_links`.
     * **overlay.mountopt**: A pipe-separated list of extra mount options. Default: `nodev`.
     * **overlay.use_composefs**: Set to true to enable the use of composefs to mount data layers with overlay. Default: `false`.
   * **b2c.load**: Load an OCI/docker archive, as generated by `podman save` or `docker save`.
     The archive can be retrieved from an HTTP URL (`b2c.load=<url>`) or loaded from a volume
     declared by `b2c.volume` (`b2c.load=<volume>/<filename>`). The container image may
     then be referenced in `b2c.run` by any of the container tags that were used when
     generating the archive. For example, if you generated it using
     `podman save -o image.tar docker.io/library/alpine:latest my-alpine:v1`, you may run it using either
     `b2c.run="-ti localhost/my-alpine:v1"` or `b2c.run="-ti docker.io/library/alpine:latest"`.
     If you don't know how the image was generated, check it out in `manifest.json`/`index.json`
     at the root of the archive.

     Warnings:
       * When using the image you imported, make sure to add `--pull=never` in your `b2c.run`
         cmdline, otherwise boot2container will fail at checking if a new version was pushed.
       * If the container tag you used to create the archive was not done using a
         fully-qualified-name (eg. `alpine:latest` instead of `docker.io/library/alpine:latest`),
         the container image will be loaded as `localhost/alpine:latest` instead of `alpine:latest`

     Examples:
       * `b2c.load=http://10.0.0.1/alpine_img.tar b2c.run="--pull=never docker.io/library/alpine:latest"`
       * `b2c.filesystem=sda1fs,type=ext4,src=/dev/sda1 b2c.volume=sda1vol,filesystem=sda1fs b2c.load=sda1vol/my_image.tar b2c.run="--pull=never localhost/testimage:latest"`

 * **Services**:
    * **b2c.ntp_peer**: Set the clock on boot by querying the specified NTP server.
     The value can be an IP address, a fully qualified name, or one of the
     following values:
      * none (default): Do not set the time at boot
      * auto: Use `pool.ntp.org` as a peer
   * **b2c.minio**: Create a minio alias, which can then be referenced in the
     `b2c.volume` specifications.
     Syntax: `b2c.minio="<alias>,<URL>,<ACCESSKEY>,<SECRETKEY>`
   * **b2c.httpd**: Start an HTTP server that allows controlling and getting
     metrics about execution through a REST interface. See [b2c.httpd REST API](#b2chttpd-rest-api)
     for the list of endpoints. Syntax: `b2c.httpd=<addr>:<port>`.
 * **Caching**:
   * **b2c.filesystem**: Define all the parameters for a filesystem which can be
     referenced by name in a `b2c.volume` or as a `b2c.cache_device`.
     Syntax: `b2c.filesystem=<name>,<opt1>,<opt2=val>,...`.
     Available options:
     * **type**: Type of the filesystem.
     * **src**: Source of the filesystem.
     * **opts**: List of pipe-separated mount options.
     Example: `b2c.filesystem=myNFS_drive,type=nfs,src=10.0.0.1:/,opts=nodev|ro b2c.cache_device=myNFS_drive`.
   * **b2c.nbd**: Configure a network block device. Syntax: `b2c.nbd=<device>,<opt1>,<opt2=val>,...`
    Available options:
      **device**: The network block device to configure, `/dev/nbd0` for example (**REQUIRED**).
      **host**: The hostname of the NBD server (**REQUIRED**).
      **port**: The port of the NBD server. Default: `10809`
      **name**: Export name of the block device. If missing, the "default" export will be selected.
      **block-size**: Use the specified block size. Acceptable values: `512`, `1024`, `2048`, and `4096`. Default: `512`
      **timeout**: Set the sockets' timeout in seconds for the NBD connections. Default: `30` seconds
      **connections**: Number of connections to make to the NBD server. Acceptable range: `1-64`. Default: `1`
      **read-only**: Ask the kernel to treat the block device as read-only.
      **swap**: Specify that this device will be used as swap space. Efforts will be taken to reduce the chances of deadlocks.
   * **b2c.cache_device**: Use a cache drive/partition to store the image layers and volumes.
     Syntax: `b2c.cache_device=<name>,<opt1>,<opt2=val>,...`
     Available options:
     * **name**: What resource should be used for caching, may be either one of these:
       * `none` (default): Do not use any cache, guaranteeing a fresh environment every time
       * `auto`: If allowed by `use=`, will re-use a previously-setup block device, create a partition in unpartitioned space, or re-partition the most suitable disk (in this order)!
       * `reset`: Same as `auto`, but will re-format the block device before use
       * `/dev/XXX`: A path to a block device (disk or partition) to limit the block devices that are considered as a cache device; it then behaves as `auto`.
       * `b2c.filesystem` name: Use a filesystem, as defined by `b2c.filesystem`. The filesystem will be used as is, without being reformated.
     * **fstrim=**: Control when boot2container should run fstrim. Possible options are:
       * never (default): Do not run fstrim
       * pipeline_start: Run fstrim as soon as the cache_device gets mounted, and wait for completion before running the first container. (**NOT RECOMMENDED**[^fstrim-warning])
     * **use=**: List of pipe-separated options specifying what the `auto` and `reset` modes can pick. Possible options are:
       * existing: Only allow boot2container to re-use existing `B2C_CACHE` partitions
       * free-space: Allow boot2container to take over un-partitioned space
       * full-disk: Allow boot2container to re-partition and format whole drives (**WILL POTENTIALLY ERASE EXISTING DATA**)
       Default: `b2c.cache_device=...,use=existing|free-space`
   * **b2c.swap**: Set up a swap space, either as a swap file stored on the cache device or by
     directly referencing a block device to use. Syntax: `b2c.swap=<name>,<opt1>,<opt2=val>`
     Available options:
       * **name**: Can be any of the following:
         * `/dev/XXX`: A path to a block device (disk or partition) that will get formated and mounted.
         * `$size`: Create a swap file on the cache partition, of the specified size in byte. Suffixes of k, m, g, t, p, e may be specified to denote KiB, MiB, GiB, etc... May be combined with `zram.enable=1`. Examples: `512M`, `16G`.
       * **swappiness**: A value between `0` and `200` defining the relative cost of swapping vs filesystem paging (see [Swappiness](https://docs.kernel.org/admin-guide/sysctl/vm.html#swappiness) for more details). Default: `60`
       * **resume**: Attempt to resume from hibernation using this swap image
   * **b2c.volume**: Create a volume that can be mounted in the container. Example: `b2c.volume=trace-cache b2c.run="-v trace-cache:/traces docker://hello-world:latest"`. Can take the following comma-separated options:
     * **filesystem**: use the specified `b2c.filesystem` as backing for the volume.
       This option takes precedence over all the other `b2c.volume` options but
       `expiration`.
     * **mirror**: defines where to pull from / push to. This is equivalent
       to setting separately `pull_from` and `push_to`. Use `pull_on` and
       `push_on` to control when the mirroring actions should happen.
       Syntax: `mirror=<alias>/bucket/folder/to/mirror`, with `<alias>`set up
       using `b2c.minio` option.
     * **pull_from** / **push_to**: Same syntax as `mirror`, but enables you to
       pull and push to different buckets/folders.
       Syntax: `pull_from=<alias>/bucket_from/,push_to=<alias>/bucket_to/`.
     * **pull_on** / **push_on**: Pipe-separated list of conditions that will trigger a
       mirror->volume or volume->mirror sync.
       Available conditions: `creation`, `pipeline_start`, `container_start`,
       `container_end`, `pipeline_end`, and `changes`.
       The `creation` condition is only available for `pull_on` and ignored for `push_on`.
       The `changes` condition overrules all the others, and will make sure all
       the files have been pulled/pushed at the end of the pipeline.
       Syntax: `pull_on=pipeline_start,push_on=container_end|pipeline_end`
     * **expiration**: Specify when the volume should get deleted:
       * `never`: Keep the volume indefinitely, if possible (default);
       * `pipeline_end`: Remove the volume when the pipeline is over;
       * More options will come later.
     * **overwrite**: Overwrite object(s) if it differs from source
     * **preserve**: Preserve file(s)/object(s) attributes and bucket(s)
       policy/locking configuration(s) on target bucket(s)
     * **remove**: Remove extraneous object(s) on either side of the mirror
     * **exclude**: Exclude object(s) that match specified object name pattern
     * **encrypt_key**: Encrypt/decrypt objects on the mirror, using
       server-side encryption with customer provided keys
     * **fscrypt_key**: Encrypt locally the volume using the provided 512-bits
       base64-encoded key (`head -c 64 /dev/urandom | base64 -w 0`).
       __WARNING__: This requires a cache device and specific
       [kernel configuration](#linux-configuration).
     * **fscrypt_reset_key**: Unless this parameter is specified, the content of
       the volume will not be deleted if the provided key does not match the
       one used previously.
 * **Shutdown**:
   * **b2c.poweroff_delay**: Delay in seconds between the
     `It's now safe to turn off your computer` message, and the execution of the
     `b2c.shutdown_cmd` command. Default: `0` seconds.
   * **b2c.shutdown_cmd**: Once the execution of all the containers is over,
     execute this command unless the container pipeline asked to reboot (see
     `b2c.reboot_cmd`). Default: `poweroff -f`
   * **b2c.reboot_cmd**: Command to execute if a container in the
     pipeline (`b2c.run` or `b2c.run_post`) exited using
     `reboot` (podman exit code 129), unless a later container (`b2c.run`
     or `b2c.run_post`) exited using `poweroff` (podman exit code 130).
     Default: `reboot -f`
 * **Deprecated options**:
   * **b2c.container**: Alias for **b2c.run** (deprecated in v0.9.9)
   * **b2c.post_container**: Alias for **b2c.run_post** (deprecated in v0.9.9)
   * **b2c.service**: Alias for **b2c.run_service** (deprecated in v0.9.9)

## Environment variables provided to containers

You may provide environment variables to your container by simply specifying it
in the command line of the container:

    b2c.run="-e VAR=VALUE -e VAR2=VALUE2 docker://hello-world"

Additionally, the following environment variables will be set, to help containers
decide what they want to do if a previous container failed:

 * `B2C_PIPELINE_STATUS`: `0` if all the containers' exit codes were 0, otherwise the exit code of the first container than failed. **NOTE**: `b2c.run_post`'s do not affect this variable;
 * `B2C_PIPELINE_FAILED_BY`: Command line of the first container that failed, or empty if `B2C_PIPELINE_STATUS` == 0. **NOTE**: `b2c.run_post`'s do not affect this variable;
 * `B2C_PIPELINE_PREV_CONTAINER`: Command line of the previous container in the pipeline, or empty if this is the first container;
 * `B2C_PIPELINE_PREV_CONTAINER_EXIT_CODE`: Exit code of the previous command, or empty if this is the first container.
 * `B2C_PIPELINE_SHUTDOWN_MODE`: Type of shutdown wanted at the end of the pipeline execution. Possible values: `default` (poweroff), `poweroff`, `reboot`. Exit your container using `reboot -f/exit 129` or `poweroff -f/exit 130` to select the wanted behaviour.

## Generating your own boot2container initramfs

First, make sure your system has the following dependencies installed:

<details><summary>Archlinux (click to expand)</summary>

    # pacman -Suy git make qemu-desktop wget yq which xz
</details>

<details><summary>Debian boomworm/Ubuntu Lunar and more recent releases (click to expand)</summary>

    # apt install --no-install-recommends git fuse-overlayfs make qemu-system wget yq xz-utils
</details>

Then, install and configure [rootless podman](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md)
(recommended) or [Docker](https://docs.docker.com/get-docker/).

### For the same architecture as your development machine

Generating an initramfs is pretty simple, provided you have already setup
docker/podman. Just run the following command to generate the initramfs (
located at `out/initramfs.linux_amd64.cpio`):

    $ make
    [...]
    07:50:04 Disabling CGO for u-root...
    07:50:04 Build environment: GOARCH=amd64 GOOS=linux GOPATH=/root/go CGO_ENABLED=0 GO111MODULE= GOROOT=/usr/lib/go PATH=/usr/lib/go/bin:$PATH
    07:50:04 NOTE: building with the new gobusybox; to get the old behavior check out commit 8b790de
    07:50:24 Successfully built "/tmp/initramfs.linux_amd64.cpio" (size 71018812).
    xz --threads=0 --check=crc32 -9 --lzma2=dict=1MiB --stdout out/initramfs.linux_amd64.cpio | dd conv=sync bs=512 of=out/initramfs.linux_amd64.cpio.xz
    38379+1 records in
    38380+0 records out
    19650560 bytes (20 MB, 19 MiB) copied, 12.985 s, 1.5 MB/s

If you want to test your initramfs, you may test it using QEMU, provided you have
downloaded a working kernel from our release page or compiled it yourself
according to the [Linux Configuration](#linux-configuration) section):

    $ make manual-test [KERNEL=/path/to/your/bzImage]

If all goes well, you should have seen the output of Docker's hello-world
image, and then a shell in an Alpine Linux container.

### Cross-compiling

Cross-compiling boot2container is relatively simple, but requires Docker/Podman
to be able to execute foreign architectures binaries
([guide for Podman on ArchLinux](https://wiki.archlinux.org/title/Podman#Foreign_architectures)).

Check that this is the case:

    $ podman run --arch arm64 'docker.io/alpine:latest' arch
    Trying to pull docker.io/library/alpine:latest...
    Getting image source signatures
    Copying blob 148d739a8e6b skipped: already exists
    Copying config 3abe2e34c9 done
    Writing manifest to image destination
    Storing signatures
    aarch64

With this done, generating the initramfs for amd64/arm64 is as simple as:

    $ make build GOARCH=(amd64|arm64)

If you would like to cross-compile for another target, use the following commands:

    $ make rebuild-container GOARCH=(arm|arm64|amd64) IMAGE_LABEL=b2c_container
    $ make build GOARCH=(arm|arm64|amd64) IMAGE_LABEL=b2c_container

You should now have your compiled initramfs located in `out/`! We do not yet
provide kernels for AArch64 or ARM v7, so make sure to compile your own
according to the [Linux Configuration](#linux-configuration) section.

## Hacking on the build container

If you would like to modify the build environment to update any binary, you
will need to re-generate the build container before generating the initramfs.

This can be done relatively easily, by running the following commands:

    $ make rebuild-container GOARCH=(arm|arm64|amd64) IMAGE_LABEL=b2c_container
    $ make build GOARCH=(arm|arm64|amd64) IMAGE_LABEL=b2c_container

That's all!

## Linux configuration

Boot2container does not contain any module, or firmware. It is thus important
to compile a kernel with everything needed, either built-in or with modules/firmware
stored in separate initramfses that can be loaded along-side boot2container by
your bootloader. Luckily, Linux is pretty good at that!

To compile a kernel compatible with boot2container, you should download the
Linux's source code from kernel.org (from git or tarballs), and extract it.
Finally, run the following command:

    $ make linux LINUX_SRC=/path/to/your/unpacked/linux [GOARCH=(arm|arm64|amd64)] [FEATURES=common,...,dgpu] [LINUX_DEFCONFIG=defconfig]

This will configure, compile Linux, and save the compiled Linux `out/linux-$GOARCH`.

A non-exhaustive list of packages you'll need to install to build the kernel
if you're running Debian/Ubuntu is:

    $ sudo apt install bc bison git make flex gcc libelf-dev libssl-dev

If the default set of features is not suiting you, you may want to select the
wanted features by setting `FEATURES=feature1,feature2,...`. The name of the
features can be found in `config/linux/` and should be separated by a comma.
The features' files will be appended to the output of Linux's
`make $LINUX_DEFCONFIG`, then squashed together using Linux's `make oldconfig`.
It will also build in the required firmware if you specify them in a feature
file using `# B2C_BUILTIN_FW+=path/*.bin`. Modules specified by
`# B2C_FEATURE_MODULES+=` will be moved to a separate cpio archive named after
the feature they are found in.

You may get prompted to review a list of config options that were set in the
config files, but were missing from the final `.config` file after
`make olddefconfig` was called. Please take the time to review if this option
is applicable to your platform. If you do not want to see this message, set
`CONFIRM=0` in your make command line!

**WARNING**: Make sure to include the `common` feature, as this is the minimum
requirement to get boot2container running!

Feel free to create your feature files, and propose them for inclusion!

### Module / firmware support

While compiling both modules and firmwares built-in makes this Linux build super
easy to use, it may also be unreasonably large. To prevent this issue, it is
possible to build non-essential drivers as modules.

This can be achieved easily: simply change the from `CONFIG_*=y` to `CONFIG_*=m`,
then drop its associated `# B2C_FW+=*` comment (if applicable).

After re-compiling the kernel using `make linux`, you will see 2 new initrds
generated:

 * `out/linux-$GOARCH.modules.cpio.xz`: Contains the modules, provides
   `/lib/modules/$KERNEL_NAME/`.
 * `out/linux-$GOARCH.firmware.cpio.xz`: Contains the firmware requested by the modules,
   provides `/lib/firmware/`.

These files can be loaded by your bootloader alongside boot2container, and the
`modules_load` kernel parameter can be used to specify the list of modules that
should be loaded before executing boot2container. As an [ipxe](https://ipxe.org/)
script for an x86_64 EFI platform, it would look like this:

    #!ipxe
    kernel /linux-x86_64 modules_load=mod1,mod2 b2c.run='-ti docker.io/library/alpine:latest`
    initrd /initramfs.linux_amd64.cpio.xz
    initrd /linux-x86_64.depmod.cpio.xz
    initrd /linux-x86_64.wifi.cpio
    boot

If your bootloader does not support loading multiple initrds (such as QEmu),
you can simply concatenate all the initrds you want to use then using the
result file as an initrd:

    $ cat initramfs.linux_amd64.cpio.xz linux-x86_64.depmod.cpio.xz linux-x86_64.wifi.cpio > combined-initrd
    $ qemu-system-x86_64 -cpu max -enable-kvm -kernel linux-x86_64 -initrd combined-initrd -nographic -m 1G -smp 4 -append 'console=ttyS0 b2c.run="-ti docker://docker.io/library/alpine:edge"'

**Known issues**:

  * [#15 - modules.devname not honored](https://gitlab.freedesktop.org/gfx-ci/boot2container/-/issues/15):
    Modules requiring the userspace to create nodes in /dev may not work (fuse, cuse, ...)
  * [#9 - late loading of firmware/modules](https://gitlab.freedesktop.org/gfx-ci/boot2container/-/issues/9):
    Firmware/modules cannot be cached between runs since they need to be loaded by the bootloader
  * [#13 - Auto-loading of modules](https://gitlab.freedesktop.org/gfx-ci/boot2container/-/issues/13):
    Modules can only be loaded using `modules_load=` which is confusing

### Out of tree modules support

If you would like to compile out-of-tree modules inside a container that is
running in boot2container, you will need to mount the headers initrd. As an
[ipxe](https://ipxe.org/) script for an x86_64 EFI platform, it would look
like this:

    #!ipxe
    kernel /linux-amd64 b2c.run='-ti docker.io/library/archlinux:latest`
    initrd /initramfs.linux_amd64.cpio.xz
    initrd /linux-amd64.modules.cpio.xz
    initrd /linux-amd64.headers.cpio.xz
    boot

This will populate the `/lib/modules/$KERNEL_VERSION/` folder with both the
modules and the kernel headers needed to build out of tree drivers.

As an example, you could install the nvidia proprietary driver using the
following commands:

    # pacman -Sy dkms nvidia-dkms
    # dkms install /usr/src/nvidia-*/dkms.conf

That's it!

## b2c.httpd REST API

All the routes exported by this server expect JSON-formatted request bodies (for `POST` requests) and return JSON-formatted bodies.

### /api/v1/status [GET]

Get basic information about the currently-running job.

Fields:

 * `version` \[int, ro\]: Current version of the API

### /api/v1/suspend [GET|POST]

Fields:

 * `state` \[string, rw\]: Content of the file `/sys/power/state`
 * `mem_sleep` \[string, rw\]: Content of the file `/sys/power/mem_sleep`
 * `disk` \[string, rw\]: Content of the file `/sys/power/disk`

See [Linux's System Sleep States](https://www.kernel.org/doc/html/v6.12/admin-guide/pm/sleep-states.html#basic-sysfs-interfaces-for-system-suspend-and-hibernation) for more information.

### /api/v1/timeouts/$kind [GET|POST|DELETE]

This endpoint is meant to get or set timeouts on various parts of the execution. Supported timeouts are:

 * `pipeline`: Indicate by which point the `b2c.run` and `b2c.run_post` containers should be done executing

Fields:

 * `active` \[bool, rw\]: A timeout is set for the execution
 * `remaining` \[float, rw\]: The number of seconds remaining before the timeout
 * `action` \[string, rw\]: The action that will be taken when timing out. Valid options:
   * `none`: No action will be taken. Useful for fully-cooperative preemption
   * `hard_reboot`: Reboot the machine immediately without syncing disks or any other teardown process (**UNSAFE**)
   * `hard_shutdown`: Shutdown the machine immediately without syncing disks or any other teardown process (**UNSAFE**)
   * `reboot`: Stop the execution of all running containers, perform the cleanups, then reboot the computer
   * `shutdown`: Stop the execution of all running containers, perform the cleanups, then shutdown the computer
   * `suspend`: Suspend the machine according to the parameters set in the `suspend` field
 * `has_timed_out` \[bool, ro\]: A convenience field that is true if the timeout is set and `remaining_seconds` is negative.
 * `suspend` \[suspend, rw\]: Parameters to use when using the `suspend` action. See `/api/v1/suspend` for more details.
 * `grace_period` \[uint, rw\]: Number of seconds given to containers to safely shutdown, before they get forcefully shutdown. Default: `10`.

### /api/v1/timeouts [GET]

This endpoint lists the different timeouts supported by boot2container along with their
current settings.

## Footnotes

[^fstrim-warning]: _Running fstrim frequently, or even using mount -o discard, might
negatively affect the lifetime of poor-quality SSD devices. For
most desktop and server systems a sufficient trimming frequency
is once a week. Note that not all devices support a queued trim,
so each trim command incurs a performance penalty on whatever
else might be trying to use the disk at the time._
([src](https://man7.org/linux/man-pages/man8/fstrim.8.html))
