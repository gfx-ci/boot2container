package filesystem

import (
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

const (
	opt_name = `b2c.filesystem`

	sampleCmdline = `b2c.filesystem=myNFS_drive,type=nfs,src=10.0.0.1:/,opts=nodev|ro b2c.filesystem=localdrive,src=/dev/sda1`

	weirdlyBrokenCmdline = `b2c.filesystem=myNFS_drive,src=test b2c.filesystem=myNFS_drive,newparam,src=test b2c.filesystem=name b2c.filesystem=`
)

func compareStringField(t *testing.T, got string, want string, field string) {
	if got != want {
		t.Fatalf("Unexpected %s mismatch: Got '%s' but wanted '%s'", field, got, want)
	}
}

func compareStringSlice(t *testing.T, got []string, want []string, fieldName string) {
	if len(got) != len(want) {
		t.Fatalf("Field %s mismatch: Expected %q elements but got %q", fieldName, want, got)
	}

	for i, val := range got {
		if val != want[i] {
			t.Fatalf("\nExpected: %q\nGot: %q\nThe argument #%d differs: Expected '%s' but got '%s'", want, got, i, val, want[i])
		}
	}
}

func checkExpectation(t *testing.T, parameters string, want map[string]FilesystemConfig) map[string]FilesystemConfig {
	opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: opt_name})
	fses := ListCmdlineFilesystems(opt)

	if len(fses) != len(want) {
		t.Fatalf("Did not get the expected amount of filesystems: Got %q but wanted %q", fses, want)
	}

	for _, fs := range fses {
		want_fs := want[fs.Name]

		compareStringField(t, fs.Name, want_fs.Name, "Name")
		compareStringField(t, fs.Type, want_fs.Type, "Type")
		compareStringField(t, fs.Src, want_fs.Src, "Src")
		compareStringField(t, fs.Source(), want_fs.Source(), "Source")
		compareStringSlice(t, fs.Opts, want_fs.Opts, "Opts")
	}

	return fses
}

func TestListCmdlineFilesystems__sampleCmdline(t *testing.T) {
	want := map[string]FilesystemConfig{
		"myNFS_drive": {Name: "myNFS_drive", Type: "nfs", Src: "10.0.0.1:/", Opts: []string{"nodev", "ro"}},
		"localdrive":  {Name: "localdrive", Type: "", Src: "/dev/sda1", Opts: []string{}},
	}
	checkExpectation(t, sampleCmdline, want)
}

func TestListCmdlineFilesystems__weirdlyBrokenCmdline(t *testing.T) {
	want := map[string]FilesystemConfig{"myNFS_drive": {Name: "myNFS_drive", Type: "", Src: "test", Opts: []string{}}}
	checkExpectation(t, weirdlyBrokenCmdline, want)
}

func TestFilesystemMount(t *testing.T) {
	fsMin := FilesystemConfig{Name: "fsMin", Src: "/dev/sda1"}
	cmd, args := fsMin.mountCmdline("/mnt/test/")
	compareStringField(t, cmd, "mount", "command")
	compareStringSlice(t, args, []string{"/dev/sda1", "/mnt/test/"}, "mountCmdline")

	fsFull := FilesystemConfig{Name: "FsFull", Src: "/dev/sda2", Type: "ext4", Opts: []string{"nodev", "ro"}}
	fsFullExpectedArgs := []string{"-t", "ext4", "/dev/sda2", "-o", "nodev,ro", "/mnt"}
	cmd, args = fsFull.mountCmdline("/mnt")
	compareStringField(t, cmd, "mount", "command")
	compareStringSlice(t, args, fsFullExpectedArgs, "mountCmdline")

	// Ensure that failures are propagated
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Fail
	if err := fsFull.Mount("/mnt"); err != nil {
		err_want := runcmd.MockExecutionError.Error()
		if err.Error() != err_want {
			t.Fatalf("Mount returned the wrong error message: Expected '%s' but got '%s'", err_want, err.Error())
		}
	} else {
		t.Fatalf("Expected Mount to return an error")
	}

	if len(runcmd.CallsHistory) != 1 {
		t.Fatalf("We expected only one call, got %q\n", runcmd.CallsHistory)
	}

	runcmd.CallsHistory[0].AssertEquals(runcmd.RunCall{Cmd: "mount", Args: fsFullExpectedArgs})
}

func TestFilesystemFormatWithoutLabel(t *testing.T) {
	// Minimal config
	cmd, args := formatCmdline("vfat", "/dev/sda1", "", []string{})
	compareStringField(t, cmd, "mkfs.vfat", "command")
	compareStringSlice(t, args, []string{"/dev/sda1"}, "formatCmdline")
}

func TestFilesystemFormatWithLabel(t *testing.T) {
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Success

	fs := FilesystemConfig{Name: "fs", Type: "ext4", Src: "/dev/sda2"}
	if err := fs.Format("LABEL", []string{"encrypt,other"}); err != nil {
		t.Fatalf("Did not expect Format to return an error")
	}

	if len(runcmd.CallsHistory) != 1 {
		t.Fatalf("We expected only one call, got %q\n", runcmd.CallsHistory)
	}

	expectedArgs := []string{"-O", "encrypt,other", "-L", "LABEL", "/dev/sda2"}
	runcmd.CallsHistory[0].AssertEquals(runcmd.RunCall{Cmd: "mkfs.ext4", Args: expectedArgs})
}

func TestFstrim(t *testing.T) {
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Success

	fs := FilesystemConfig{}
	if err := fs.Fstrim("/mount/point"); err != nil {
		t.Fatalf("Did not expect Fstrim to return an error")
	}

	if len(runcmd.CallsHistory) != 1 {
		t.Fatalf("We expected only one call, got %q\n", runcmd.CallsHistory)
	}

	expectedArgs := []string{"-v", "/mount/point"}
	runcmd.CallsHistory[0].AssertEquals(runcmd.RunCall{Cmd: "fstrim", Args: expectedArgs})
}

func TestFilesystemIsValid(t *testing.T) {
	fs := FilesystemConfig{}
	if fs.IsValid() {
		t.Fatalf("An empty filesystem shouldn't be valid")
	}

	fs = FilesystemConfig{Name: "Hello"}
	if fs.IsValid() {
		t.Fatalf("A filesystem without source shouldn't be valid")
	}

	fs = FilesystemConfig{Name: "Hello", Src: "/dev/sda1"}
	if fs.IsValid() == false {
		t.Fatalf("A filesystem with both a name and a source should be considered valid")
	}
}

func TestFilesystemString(t *testing.T) {
	fs := FilesystemConfig{Name: "MyFS", Src: "/dev/sda1", Type: "ext4", Opts: []string{"nodev", "ro"}}
	want := "<Filesystem: Name='MyFS' Type='ext4' Src='/dev/sda1' Opts=[\"nodev\" \"ro\"]>"
	if fs.String() != want {
		t.Fatalf("Unexpected string representation: Expected '%s' but got '%s'\n", want, fs)
	}
}

// Equality tests

var (
	fsOrig = FilesystemConfig{Name: "Name", Src: "/dev/sda1", Type: "ext4", Opts: []string{"nodev", "ro"}}
)

func dupFs(o FilesystemConfig) FilesystemConfig {
	n := o

	n.Opts = make([]string, len(o.Opts))
	copy(n.Opts, o.Opts)

	return n
}

func checkEqual(t *testing.T, fs1 FilesystemConfig, fs2 FilesystemConfig, want bool) {
	got := fs1.Equal(fs2)
	if got != want {
		t.Fatalf("Unexpected return value:\nfs1: %s\nfs2: %s\nExpected: %t\nGot: %t",
			fs1.String(), fs2.String(), want, got)
	}
}

func TestFilesystemEqual__Equality(t *testing.T) {
	fs := dupFs(fsOrig)
	checkEqual(t, fsOrig, fs, true)
}

func TestFilesystemEqual__LastOptModified(t *testing.T) {
	fs := dupFs(fsOrig)
	fs.Opts[1] = "ro-mod"
	checkEqual(t, fsOrig, fs, false)
}

func TestFilesystemEqual__ExtraOption(t *testing.T) {
	fs := dupFs(fsOrig)
	fs.Opts = append(fs.Opts, "new")
	checkEqual(t, fsOrig, fs, false)
}

func TestFilesystemEqual__SrcChanged(t *testing.T) {
	fs := dupFs(fsOrig)
	fs.Src = "/dev/helloworld"
	checkEqual(t, fsOrig, fs, false)
}

func TestFilesystemEqual__TypeChanged(t *testing.T) {
	fs := dupFs(fsOrig)
	fs.Type = "ext42"
	checkEqual(t, fsOrig, fs, false)
}

func TestFilesystemEqual__NameChanged(t *testing.T) {
	fs := dupFs(fsOrig)
	fs.Name = "nAmE"
	checkEqual(t, fsOrig, fs, false)
}
