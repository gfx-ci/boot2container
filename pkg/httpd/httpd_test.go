package httpd

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
)

func assertEqual(t *testing.T, field string, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s: `%v` != `%v`", field, a, b)
	}
}

// --------------------------------- Actions ----------------------------------

func TestAction(t *testing.T) {
	assertEqual(t, "Empty action", ToAction(""), ActionNone)
	assertEqual(t, "Invalid action id", int(ToAction("invalid")), -1)
	assertEqual(t, "Invalid action string", Action(42).String(), "")

	// Iterate through all the valid actions and make sure we can go from
	// action to string to action safely
	for i := ActionNone; i < ActionLast; i = i + 1 {
		assertEqual(t, fmt.Sprintf("Action %d parsing", i),
			int(ToAction(Action(i).String())), int(i))
	}
}

// ----------------------------- Cmdline parsing ------------------------------

func TestParseCmdline__empty(t *testing.T) {
	opt := cmdline.FindOptionInString("", cmdline.OptionQuery{Name: "b2c.httpd"})

	want := HttpdConfig{ListenAddr: ""}

	got := ParseCmdline(nil)
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Expected '%v', but got '%v'", want, got)
	}

	got = ParseCmdline(opt)
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Expected '%v', but got '%v'", want, got)
	}
}

func TestParseCmdline__full_test(t *testing.T) {
	opt := cmdline.FindOptionInString("b2c.httpd=helloworld,extra,args", cmdline.OptionQuery{Name: "b2c.httpd"})
	got := ParseCmdline(opt)

	want := HttpdConfig{ListenAddr: "helloworld"}
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Expected '%v', but got '%v'", want, got)
	}
}
