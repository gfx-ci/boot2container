package httpd

import (
	"fmt"
	"strings"
	"time"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
)

const (
	OPT_NAME = "b2c.httpd"
)

// Timeouts
type Action int

const (
	ActionNone Action = iota
	ActionHardReboot
	ActionHardShutdown
	ActionReboot
	ActionShutdown
	ActionSuspend
	ActionLast
)

func (s Action) String() string {
	statuses := [...]string{"none", "hard_reboot", "hard_shutdown", "reboot", "shutdown", "suspend"}
	if len(statuses) < int(s) || s >= ActionLast {
		return ""
	}
	return statuses[s]
}

func ToAction(b string) Action {
	switch strings.ToLower(b) {
	case "", "none":
		return ActionNone
	case "hard_reboot":
		return ActionHardReboot
	case "hard_shutdown":
		return ActionHardShutdown
	case "reboot":
		return ActionReboot
	case "shutdown":
		return ActionShutdown
	case "suspend":
		return ActionSuspend
	default:
		return -1
	}
}

type SuspendV1 struct {
	State    string `json:"state"`
	MemSleep string `json:"mem_sleep"`
	Disk     string `json:"disk"`
}

type TimeoutConfig struct {
	Timeout      time.Time
	TimeoutTimer *time.Timer

	Action      Action
	Suspend     SuspendV1
	GracePeriod time.Duration
}

type TimeoutsConfig struct {
	Pipeline TimeoutConfig
}

// ParseCmdLine

type HttpdConfig struct {
	ListenAddr string

	Timeouts TimeoutsConfig
}

func ParseCmdline(opt *cmdline.Option) HttpdConfig {
	cfg := HttpdConfig{}

	var val *cmdline.OptionValue
	if opt == nil {
		val = nil
	} else {
		val = opt.Value()
	}

	if val == nil || len(val.Args) == 0 {
		return cfg
	} else if len(val.Args) > 1 {
		fmt.Printf("WARNING: The following `%s` arguments are unsupported: %q\n", OPT_NAME, val.Args[1:])
	}

	cfg.ListenAddr = val.Args[0]

	return cfg
}
