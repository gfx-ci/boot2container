package httpd

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

const (
	CONTAINER_EXIT_CODE_OVERRIDE_PATH = "/run/b2c/container_exit_code_override"
)

// ------------------------------ /api/v1/status ------------------------------

type StatusV1 struct {
	Version int `json:"version"`

	// TODO: Add the b2c version, kernel version, uptime, ...
}

func (h *HttpdConfig) statusV1Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(StatusV1{Version: 1})
}

// ----------------------------- /api/v1/suspend ------------------------------

func readFile(path string) string {
	data, err := os.ReadFile(path)
	if err != nil {
		fmt.Printf("WARNING: Failed to read - %s\n", err.Error())
		return ""
	}

	return strings.TrimSuffix(string(data), "\n")
}

func doSuspend(p SuspendV1) (errors []string) {
	errors = make([]string, 0)
	if err := os.WriteFile("/sys/power/mem_sleep", []byte(p.MemSleep), 0666); err != nil {
		errors = append(errors, err.Error())
	}
	if err := os.WriteFile("/sys/power/disk", []byte(p.Disk), 0666); err != nil {
		errors = append(errors, err.Error())
	}
	if err := os.WriteFile("/sys/power/state", []byte(p.State), 0666); err != nil {
		errors = append(errors, err.Error())
	}

	return errors
}

func (h *HttpdConfig) suspendV1GetHandler(w http.ResponseWriter, _ *http.Request) {
	ret := SuspendV1{
		State:    readFile("/sys/power/state"),
		MemSleep: readFile("/sys/power/mem_sleep"),
		Disk:     readFile("/sys/power/disk"),
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ret)
}

func (h *HttpdConfig) suspendV1PostHandler(w http.ResponseWriter, r *http.Request) {
	var p SuspendV1
	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading request body", http.StatusInternalServerError)
		return
	}
	if err := json.Unmarshal(body, &p); err != nil {
		errorMsg := fmt.Sprintf("Error parsing request body: %s", err.Error())
		http.Error(w, errorMsg, http.StatusBadRequest)
		return
	}

	if errors := doSuspend(p); len(errors) != 0 {
		http.Error(w, "Failed to write the wanted configuration:\n\n * "+strings.Join(errors, "\n * "),
			http.StatusBadRequest)
		return
	}
}

func (h *HttpdConfig) suspendV1Handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.suspendV1GetHandler(w, r)
	case "POST":
		h.suspendV1PostHandler(w, r)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

// ----------------------------- /api/v1/timeouts/ ----------------------------

type TimeoutV1 struct {
	Active    bool    `json:"active"`
	Remaining float64 `json:"remaining"`
	Action    string  `json:"action"`

	// Read-only
	HasTimedOut bool `json:"has_timed_out"`

	// Action-related fields
	Suspend     SuspendV1 `json:"suspend"`
	GracePeriod uint      `json:"grace_period"`
}

func timeoutV1Serializer(timeoutCfg *TimeoutConfig) TimeoutV1 {
	now := time.Now()

	ret := TimeoutV1{
		Active:      !timeoutCfg.Timeout.IsZero(),
		Remaining:   timeoutCfg.Timeout.Sub(now).Seconds(),
		Action:      timeoutCfg.Action.String(),
		HasTimedOut: !timeoutCfg.Timeout.IsZero() && timeoutCfg.Timeout.Before(now),

		Suspend:     timeoutCfg.Suspend,
		GracePeriod: uint(timeoutCfg.GracePeriod.Seconds()),
	}

	// Clean up the remaining seconds if the timer is not active
	if !ret.Active {
		ret.Remaining = 0.0
	}

	return ret
}

func timeoutV1GetHandler(w http.ResponseWriter, _ *http.Request, timeoutCfg *TimeoutConfig) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(timeoutV1Serializer(timeoutCfg))
}

func printToConsole(msg string) {
	os.WriteFile("/dev/console", []byte(msg), 0o644)
}

func overrideContainerExitCode(exitCode int, stopTimeout time.Duration) (errors []string) {
	errors = make([]string, 0)

	if err := os.MkdirAll(filepath.Dir(CONTAINER_EXIT_CODE_OVERRIDE_PATH), 0o755); err != nil {
		errors = append(errors, "Failed to create the b2c run folder: %s", err.Error())
	}

	if err := os.WriteFile(CONTAINER_EXIT_CODE_OVERRIDE_PATH, []byte(strconv.Itoa(exitCode)), 0o644); err != nil {
		errors = append(errors, "Failed to override the exit code: %s", err.Error())
	}

	if err := runcmd.Run("podman", []string{"stop", "-a", "-t", strconv.Itoa(int(stopTimeout.Seconds()))}); err != nil {
		errors = append(errors, "Podman stop failed: %s", err.Error())
	}

	return
}

func performTimeoutAction(timeoutName string, timeoutCfg *TimeoutConfig) {
	printToConsole(fmt.Sprintf("\nWARNING: Exceeded the '%s' timeout, performing timeout action '%s'\n",
		timeoutName, timeoutCfg.Action.String()))

	var errors []string
	switch timeoutCfg.Action {
	case ActionNone:
		// Do nothing
	case ActionReboot:
		errors = overrideContainerExitCode(129, timeoutCfg.GracePeriod)
	case ActionShutdown:
		errors = overrideContainerExitCode(130, timeoutCfg.GracePeriod)
	case ActionSuspend:
		// Reset the timeout so that we may queue another one on resume
		timeoutCfg.TimeoutTimer = nil

		errors = doSuspend(timeoutCfg.Suspend)
	case ActionHardShutdown:
		if err := syscall.Reboot(syscall.LINUX_REBOOT_CMD_POWER_OFF); err != nil {
			errors = []string{err.Error()}
		}
	case ActionHardReboot:
		if err := syscall.Reboot(syscall.LINUX_REBOOT_CMD_RESTART); err != nil {
			errors = []string{err.Error()}
		}
	}

	// Print an error message to let us know something happened
	if len(errors) > 0 {
		errorMsg := "ERROR: Failed to perform the timeout action:\n\n * " + strings.Join(errors, "\n * ")
		printToConsole(errorMsg)
	}
}

func timeoutV1PostHandler(w http.ResponseWriter, r *http.Request, timeoutName string, timeoutCfg *TimeoutConfig) {
	var p TimeoutV1
	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading request body", http.StatusInternalServerError)
		return
	}
	if err := json.Unmarshal(body, &p); err != nil {
		errorMsg := fmt.Sprintf("Error parsing request body: %s", err.Error())
		http.Error(w, errorMsg, http.StatusBadRequest)
		return
	}

	if timeoutCfg.TimeoutTimer != nil && !timeoutCfg.TimeoutTimer.Stop() {
		http.Error(w, "Can't update a timeout that already timed out", http.StatusBadRequest)
		return
	}

	// Parse the action
	if action := ToAction(p.Action); action >= 0 {
		timeoutCfg.Action = action
		timeoutCfg.Suspend = p.Suspend
	} else {
		http.Error(w, fmt.Sprintf("Unsupported action '%s'", p.Action), http.StatusBadRequest)
		return
	}

	if !p.Active {
		timeoutCfg.Timeout = time.Time{}
	} else {
		if p.Remaining < 0.0 {
			http.Error(w, "ERROR: Remaining can't be negative", http.StatusBadRequest)
			return
		}

		timeout := time.Duration(p.Remaining * float64(time.Second))
		timeoutCfg.Timeout = time.Now().Add(timeout)
		timeoutCfg.TimeoutTimer = time.AfterFunc(timeout, func() { performTimeoutAction(timeoutName, timeoutCfg) })
	}

	// Parse the number of seconds given to containers to stop on their own
	if p.GracePeriod > 0 {
		timeoutCfg.GracePeriod = time.Duration(p.GracePeriod) * time.Second
	} else {
		timeoutCfg.GracePeriod = 10 * time.Second
	}

	// Return the new timeout value
	timeoutV1GetHandler(w, r, timeoutCfg)
}

func timeoutV1DeleteHandler(w http.ResponseWriter, r *http.Request, timeoutCfg *TimeoutConfig) {
	if timeoutCfg.TimeoutTimer != nil && !timeoutCfg.TimeoutTimer.Stop() {
		http.Error(w, "Can't delete a timeout that already timed out", http.StatusBadRequest)
		return
	}
	timeoutCfg.Timeout = time.Time{}
	timeoutCfg.TimeoutTimer = nil

	// Return the new timeout value
	timeoutV1GetHandler(w, r, timeoutCfg)
}

func timeoutV1Handler(w http.ResponseWriter, r *http.Request, timeoutName string, timeoutCfg *TimeoutConfig) {
	switch r.Method {
	case "GET":
		timeoutV1GetHandler(w, r, timeoutCfg)
	case "POST":
		timeoutV1PostHandler(w, r, timeoutName, timeoutCfg)
	case "DELETE":
		timeoutV1DeleteHandler(w, r, timeoutCfg)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (h *HttpdConfig) timeoutV1PipelineHandler(w http.ResponseWriter, r *http.Request) {
	timeoutV1Handler(w, r, "pipeline", &h.Timeouts.Pipeline)
}

type TimeoutsV1 struct {
	Pipeline TimeoutV1 `json:"pipeline"`
}

func (h *HttpdConfig) timeoutsV1GetHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(TimeoutsV1{Pipeline: timeoutV1Serializer(&h.Timeouts.Pipeline)})
}

// Run

func (h *HttpdConfig) Run() error {
	// Do nothing if no options are specified
	if h.ListenAddr == "" {
		return nil
	}

	http.HandleFunc("/api/v1/status", h.statusV1Handler)
	http.HandleFunc("/api/v1/suspend", h.suspendV1Handler)
	http.HandleFunc("/api/v1/timeouts", h.timeoutsV1GetHandler)
	http.HandleFunc("/api/v1/timeouts/pipeline", h.timeoutV1PipelineHandler)

	// TODO: Add a simple metrics endpoint compatible with influxdb that would
	// provide basic metrics such as uptime, load average, ram usage, ...

	fmt.Printf("Starting the HTTP server on %s\n", h.ListenAddr)
	return http.ListenAndServe(h.ListenAddr, nil)
}
