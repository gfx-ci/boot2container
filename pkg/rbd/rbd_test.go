package rbd

import (
	"fmt"
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
)

func assertEqual(t *testing.T, field string, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s: `%v` != `%v`", field, a, b)
	}
}

// ------------------------------ RbdClientMock -------------------------------

type RbdClientMock struct {
	name string

	setup          error
	setupCallCount int

	teardown          error
	teardownCallCount int
}

func (c *RbdClientMock) Name() string {
	return c.name
}

func (c *RbdClientMock) Setup() error {
	c.setupCallCount = c.setupCallCount + 1
	fmt.Printf("Setup() called: count = %d\n", c.setupCallCount)
	return c.setup
}

func (c *RbdClientMock) Teardown() error {
	c.teardownCallCount = c.teardownCallCount + 1
	return c.teardown
}

// ---------------------------------- Setup -----------------------------------

func TestSetup__with_failure(t *testing.T) {
	RbdClientMockSuccess := RbdClientMock{name: "/dev/nbd0", setup: nil, teardown: nil}
	RbdClientMockFail := RbdClientMock{name: "/dev/nbd1", setup: fmt.Errorf("Setup fail")}

	cfg := RbdConfig{clients: []RbdClient{&RbdClientMockSuccess, &RbdClientMockFail, &RbdClientMockSuccess}}
	got := cfg.Setup()

	assertEqual(t, "Setup() return value", got.Error(), `=> Failed to setup /dev/nbd1: Setup fail`)
	assertEqual(t, "RbdClientMockSuccess.Setup() call count", RbdClientMockSuccess.setupCallCount, 1)
	assertEqual(t, "RbdClientMockFail.Setup() call count", RbdClientMockFail.setupCallCount, 1)
}

func TestSetup__success(t *testing.T) {
	RbdClientMockSuccess := RbdClientMock{name: "/dev/nbd0", setup: nil, teardown: nil}
	got := RbdConfig{clients: []RbdClient{&RbdClientMockSuccess, &RbdClientMockSuccess}}.Setup()

	assertEqual(t, "Setup() return value", got, nil)
	assertEqual(t, "RbdClientMockSuccess.Setup() call count", RbdClientMockSuccess.setupCallCount, 2)
}

// --------------------------------- Teardown ---------------------------------

func TestTeardown__with_failure(t *testing.T) {
	RbdClientMockSuccess := RbdClientMock{name: "/dev/nbd0", setup: nil, teardown: nil}
	RbdClientMockFail := RbdClientMock{name: "/dev/nbd1", teardown: fmt.Errorf("Teardown fail")}

	cfg := RbdConfig{clients: []RbdClient{&RbdClientMockSuccess, &RbdClientMockFail, &RbdClientMockSuccess}}
	got := cfg.Teardown()

	assertEqual(t, "Setup() return value", got.Error(), `=> Failed to tear down /dev/nbd1: Teardown fail`)
	assertEqual(t, "RbdClientMockSuccess.Teardown() call count", RbdClientMockSuccess.teardownCallCount, 1)
	assertEqual(t, "RbdClientMockFail.Teardown() call count", RbdClientMockFail.teardownCallCount, 1)
}

func TestTeardown__success(t *testing.T) {
	RbdClientMockSuccess := RbdClientMock{name: "/dev/nbd0", setup: nil, teardown: nil}
	got := RbdConfig{clients: []RbdClient{&RbdClientMockSuccess, &RbdClientMockSuccess}}.Teardown()

	assertEqual(t, "Teardown() return value", got, nil)
	assertEqual(t, "RbdClientMockSuccess.Teardown() call count", RbdClientMockSuccess.teardownCallCount, 2)
}

// ----------------------------- Cmdline parsing ------------------------------

func parseNbdCmdlineMock(opt *cmdline.Option) []NbdClientConfig {
	return []NbdClientConfig{fullCfg}
}

func TestParseCmdline(t *testing.T) {
	// Mock ParseNbdCmdline
	ParseNbdCmdline = parseNbdCmdlineMock
	defer func() { ParseNbdCmdline = parseNbdCmdline }()
	_ = ParseNbdCmdline // Silence a stupid compiler warning

	nbdOpt := cmdline.FindOptionInString("", cmdline.OptionQuery{Name: "b2c.nbd"})
	cfg := ParseCmdline(nbdOpt)
	assertEqual(t, "Number of clients returned by ParseCmdline", len(cfg.clients), 1)
}
