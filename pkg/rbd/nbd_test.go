package rbd

import (
	"reflect"
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/swap"
)

var (
	fullCfg = NbdClientConfig{Device: "/dev/nbd0", Host: "10.42.0.1", BlockSize: 1024, Connections: 42,
		ExportName: "ExportName", Port: 12345, ReadOnly: true, Swap: true, Timeout: 2.5}
)

func getBlockDeviceMajorMinorMock(path string) (int, int, error) {
	if path != "/dev/nbd0" && path != "/dev/nbd1" {
		panic("Unexpected path")
	}
	return 42, 1337, nil
}

// ----------------------------------- Name -----------------------------------

func TestName(t *testing.T) {
	assertEqual(t, "Fullcfg's name", fullCfg.Name(), "nbd0")
}

// ---------------------------------- Setup -----------------------------------

func TestNbdSetup(t *testing.T) {
	// Setup the mock history
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Fail

	// Run the setup command
	got := fullCfg.Setup()
	assertEqual(t, "Setup return value", got.Error(), "Mock execution error")

	// Check the calls
	assertEqual(t, "Call count", len(runcmd.CallsHistory), 1)
	wanted := runcmd.RunCall{"nbd-client",
		[]string{"10.42.0.1", "12345", "/dev/nbd0", "-persist", "-connections", "42", "-timeout", "2.50",
			"-block-size", "1024", "-Name", "ExportName", "-readonly", "-swap"}}
	runcmd.CallsHistory[0].AssertEquals(wanted)
}

// ---------------------------------- Teardown --------------------------------

func TestNbdTeardown(t *testing.T) {
	// Setup the mock history
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Fail

	// Run the setup command
	got := fullCfg.Teardown()
	assertEqual(t, "Setup return value", got.Error(), "Mock execution error")

	// Check the calls
	assertEqual(t, "Call count", len(runcmd.CallsHistory), 1)
	runcmd.CallsHistory[0].AssertEquals(runcmd.RunCall{"nbd-client", []string{"-disconnect", "/dev/nbd0"}})
}

// ----------------------------- Cmdline parsing ------------------------------

func TestParseNbdCmdline__option_missing(t *testing.T) {
	got := parseNbdCmdline(nil)
	assertEqual(t, "Number of clients returned by parseNbdCmdline()", len(got), 0)
}

func checkExpectation(t *testing.T, parameters string, want []NbdClientConfig) {
	opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: "b2c.nbd"})

	got := parseNbdCmdline(opt)

	if len(got) != len(want) {
		t.Fatalf("Did not get the expected amount of clients.\nExpected: %v\n     Got: %v\n", want, got)
	}

	for i, wantedCfg := range want {
		if !reflect.DeepEqual(got[i], wantedCfg) {
			t.Fatalf("[%d]: Expected '%v', but got '%v'", i, want, got)
		}
	}
}

func TestParseNbdCmdline__trip_all_invalid_fields(t *testing.T) {
	checkExpectation(t, `b2c.nbd=/dev/missing,block-size=384,connections=0,port=0,timeout=-1,unknown`,
		[]NbdClientConfig{})
}

func TestParseNbdCmdline(t *testing.T) {
	// Mock getBlockDeviceMajorMinor to always return 42, 1337, nil... or panic if not called on /dev/sda like expected
	getBlockDeviceMajorMinorSaved := swap.GetBlockDeviceMajorMinor
	swap.GetBlockDeviceMajorMinor = getBlockDeviceMajorMinorMock
	defer func() { swap.GetBlockDeviceMajorMinor = getBlockDeviceMajorMinorSaved }()

	checkExpectation(t, `b2c.nbd=/dev/nbd0,host=10.42.0.1,block-size=1024,connections=42,name=ExportName,port=12345,timeout=2.5,read-only,swap b2c.nbd=/dev/nbd1,host=ci-gateway`,
		[]NbdClientConfig{fullCfg,
			NbdClientConfig{Device: "/dev/nbd1", Host: "ci-gateway", BlockSize: 512, Connections: 1, ExportName: "", Port: 10809, ReadOnly: false, Swap: false, Timeout: 30}})
}
