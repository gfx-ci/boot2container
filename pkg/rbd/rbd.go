package rbd

import (
	"fmt"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
)

var (
	ParseNbdCmdline = parseNbdCmdline
)

type RbdClient interface {
	Name() string
	Setup() error
	Teardown() error
}

type RbdConfig struct {
	clients []RbdClient
}

func (c RbdConfig) Setup() error {
	if len(c.clients) > 0 {
		fmt.Printf("# Setting up %d remote block devices:\n\n", len(c.clients))
		for _, client := range c.clients {
			fmt.Printf("  * Setting up %s:\n", client.Name())
			if err := client.Setup(); err != nil {
				return fmt.Errorf("=> Failed to setup %s: %w", client.Name(), err)
			}
		}
	}

	return nil
}

func (c RbdConfig) Teardown() error {
	if len(c.clients) > 0 {
		fmt.Printf("# Tearing down %d remote network block:\n\n", len(c.clients))
		for _, client := range c.clients {
			fmt.Printf("  * Tear down %s:\n", client.Name())
			if err := client.Teardown(); err != nil {
				return fmt.Errorf("=> Failed to tear down %s: %w", client.Name(), err)
			}
		}
	}

	return nil
}

func ParseCmdline(nbdOpt *cmdline.Option) RbdConfig {
	cfg := RbdConfig{}

	cfg.clients = make([]RbdClient, 0)

	// Add all the NBD clients
	for _, client := range ParseNbdCmdline(nbdOpt) {
		cfg.clients = append(cfg.clients, client)
	}

	// TODO: Add support for iSCSI and NVME-TCP

	return cfg
}
