package swap

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

const (
	MiB = 1024 * 1024
)

func assertEqual(t *testing.T, field string, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s: `%v` != `%v`", field, a, b)
	}
}

func getBlockDeviceMajorMinorMock(path string) (int, int, error) {
	if path != "/dev/sda" {
		panic("Unexpected path")
	}
	return 42, 1337, nil
}

// --------------------------------- Swap On ----------------------------------

func TestSwapon__do_nothing(t *testing.T) {
	// Do nothing when the swap size is 0
	got := SwapConfig{Method: MethodNone}.Swapon()
	assertEqual(t, "Swapon return value", got, nil)
}

func TestSwapon__zero_sized_swapfile(t *testing.T) {
	// Do nothing when the swap size is 0
	got := SwapConfig{Method: MethodSwapFile, SwapTarget: "/swapfile", Size: 0}.Swapon()
	assertEqual(t, "Swapon return value", got, nil)
}

func TestSwapon__backing_swapfile_creation_fails(t *testing.T) {
	err := SwapConfig{Method: MethodSwapFile, SwapTarget: "/missing/file", Size: 1}.Swapon()
	assertEqual(t, "Swapon return value", err.Error(),
		"Failed to open/create the swap file: open /missing/file: no such file or directory")
}

func testSwapon(t *testing.T, filepath string, swapSize int64, mockCalls runcmd.RunMockCallback, wantedCalls []runcmd.RunCall, want error) {
	// Setup the mock history
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = mockCalls

	// Perform the call
	err := SwapConfig{Method: MethodSwapFile, SwapTarget: filepath, Size: swapSize, Swappiness: 51}.Swapon()
	if err != nil && want != nil {
		assertEqual(t, "Swapon return value", err.Error(), want.Error())
	} else if (err == nil && want != nil) || (err != nil && want == nil) {
		t.Fatalf("Swapon return value: Expected `%v` but got `%v`", want, err)
	}

	// Check the calls
	assertEqual(t, "Call count", len(runcmd.CallsHistory), len(wantedCalls))
	for i, _ := range wantedCalls {
		runcmd.CallsHistory[i].AssertEquals(wantedCalls[i])
	}
}

func TestSwapon__fallocate_fails(t *testing.T) {
	// Create a temp file for our testing
	f, err := os.CreateTemp("", "swapfile")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer os.Remove(f.Name())

	testSwapon(t, f.Name(), -1, runcmd.Success, []runcmd.RunCall{},
		fmt.Errorf("Failed to fallocate the swap file: invalid argument"))
}

func MockSuccess(mountpoint string) error {
	return nil
}

func MockFail(mountpoint string) error {
	return fmt.Errorf("Failure!")
}

func TestSwapon(t *testing.T) {
	// Create a temp file for our testing
	f, err := os.CreateTemp("", "swapfile")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer os.Remove(f.Name())

	// mkswap call fails
	wantedCalls := []runcmd.RunCall{{"mkswap", []string{f.Name()}}}
	testSwapon(t, f.Name(), 50*MiB, runcmd.Fail, wantedCalls,
		fmt.Errorf("Failed to format the swap space %s: Mock execution error", f.Name()))

	// Mkswap succeeds, but SwapOn fails
	SwapOn = MockFail
	testSwapon(t, f.Name(), 50*MiB, runcmd.Success, wantedCalls,
		fmt.Errorf("Failed to mount the swap space: Failure!"))

	// Swapon success, but swappiness fails
	SwappinessProcfsPath = "/invalid/path"
	SwapOn = MockSuccess
	testSwapon(t, f.Name(), 50*MiB, runcmd.Success, wantedCalls, nil)

	// Everything worked!
	swappiness, err := os.CreateTemp("", "swappiness")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer os.Remove(f.Name())
	SwappinessProcfsPath = swappiness.Name()
	testSwapon(t, f.Name(), 50*MiB, runcmd.Success, wantedCalls, nil)

	// Make sure the data written to the swappiness file is the wanted one
	data, err := os.ReadFile(SwappinessProcfsPath)
	assertEqual(t, "ReadFile SwappinessProcfsPath error", err, nil)
	assertEqual(t, "SwappinessProcfsPath data", string(data), "51\n")
}

// --------------------------------- Swap Off ---------------------------------

func TestSwapoff(t *testing.T) {
	// Create a temp file for our testing
	f, err := os.CreateTemp("", "swapfile")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer os.Remove(f.Name())

	cfg := SwapConfig{Method: MethodSwapFile, SwapTarget: f.Name(), Size: 50 * MiB}

	// Fail to unmount the swap
	SwapOff = MockFail
	err = cfg.Swapoff()
	assertEqual(t, "Swapoff return value", err.Error(), "Failed to unmount the swap space: Failure!")
	_, err = os.Stat(f.Name())
	assertEqual(t, "Swap file is present", err, nil)

	// Succeed in unmounting the swap
	SwapOff = MockSuccess
	err = cfg.Swapoff()
	assertEqual(t, "Swapoff return value", err, nil)
	_, err = os.Stat(f.Name())
	assertEqual(t, "Swap file is missing", err.Error(), fmt.Sprintf("stat %s: no such file or directory", f.Name()))

	// Failure to remove the file (the file was already removed)
	err = cfg.Swapoff()
	assertEqual(t, "Swapoff return value", err.Error(),
		fmt.Sprintf("Failed to remove the swap file: remove %s: no such file or directory", f.Name()))
	_, err = os.Stat(f.Name())
	assertEqual(t, "Swap file is missing", err.Error(), fmt.Sprintf("stat %s: no such file or directory", f.Name()))
}

// ------------------------------ AttemptResumeIfApplicable -------------------------------

func TestAttemptResumeIfApplicable__swapfile_method_does_nothing(t *testing.T) {
	unsupportedMethods := []SwapMethod{MethodNone, MethodSwapFile}
	for _, unsupportedMethod := range unsupportedMethods {
		assertEqual(t, "AttemptResumeIfApplicable return value",
			SwapConfig{Method: unsupportedMethod, Resume: true}.AttemptResumeIfApplicable(), nil)
	}
}

func TestAttemptResumeIfApplicable__resume_disabled_does_nothing(t *testing.T) {
	assertEqual(t, "AttemptResumeIfApplicable return value",
		SwapConfig{Method: MethodBlockDevice, Resume: false}.AttemptResumeIfApplicable(), nil)
}

func TestAttemptResumeIfApplicable__invalid_block_device(t *testing.T) {
	GetBlockDeviceMajorMinor = func(path string) (int, int, error) { return 0, 0, fmt.Errorf("Mock error") }
	defer func() { GetBlockDeviceMajorMinor = getBlockDeviceMajorMinor }()

	err := SwapConfig{Method: MethodBlockDevice, Resume: true}.AttemptResumeIfApplicable()
	assertEqual(t, "AttemptResumeIfApplicable return value", err.Error(),
		"Failed to get the major:minor of the block device specified by `b2c.swap`: Mock error\n")
}

func TestAttemptResumeIfApplicable__success(t *testing.T) {
	// Mock getBlockDeviceMajorMinor to always return 42, 1337, nil... or panic if not called on /dev/sda like expected
	GetBlockDeviceMajorMinor = getBlockDeviceMajorMinorMock
	defer func() { GetBlockDeviceMajorMinor = getBlockDeviceMajorMinor }()

	// Run AttemptResumeIfApplicable, and expect failure of writing to the resume block device
	ResumeSysfsPath = "/invalid/path"
	err := SwapConfig{Method: MethodBlockDevice, SwapTarget: "/dev/sda", Resume: true}.AttemptResumeIfApplicable()
	assertEqual(t, "AttemptResumeIfApplicable return value", err.Error(),
		"Failed to write the resume block device to /invalid/path: open /invalid/path: no such file or directory\n")

	// Replace the resume sysfs path to a temporary file
	f, err := os.CreateTemp("", "swapfile")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer os.Remove(f.Name())
	ResumeSysfsPath = f.Name()

	// Run AttemptResumeIfApplicable, and ensure no error was returned
	assertEqual(t, "AttemptResumeIfApplicable return value",
		SwapConfig{Method: MethodBlockDevice, SwapTarget: "/dev/sda", Resume: true}.AttemptResumeIfApplicable(), nil)

	// Make sure the data written to the resume sysfs file is the wanted one
	data, err := os.ReadFile(ResumeSysfsPath)
	assertEqual(t, "ReadFile ResumeSysfsPath error", err, nil)
	assertEqual(t, "ResumeSysfsPath data", string(data), "42:1337\n")
}

// -------------------------------- Parse size --------------------------------

func checkParseSize(t *testing.T, val string, wantVal int64, wantErr string) {
	ret, err := parseSize(val)

	assertEqual(t, "Return value", ret, wantVal)

	if wantErr != "" {
		assertEqual(t, "Return error", err.Error(), wantErr)
	} else if err != nil {
		t.Fatalf("Got an unexpected error: %s", err.Error())
	}
}

func TestParseSize(t *testing.T) {
	// Test all the prefixes
	checkParseSize(t, "42", 42, "")
	checkParseSize(t, "42k", 43008, "")
	checkParseSize(t, "42MiB", 44040192, "")
	checkParseSize(t, "42GB", 42000000000, "")
	checkParseSize(t, "42TB", 42000000000000, "")
	checkParseSize(t, "42PB", 42000000000000000, "")

	// Invalid values
	checkParseSize(t, "PiB", 0, "Invalid size format: No digits found in 'PiB'")
	checkParseSize(t, "1.2.3.4", 0, "The number part (1.2.3.4) could not get parsed")
	checkParseSize(t, "-42", 0, "The specified size (-42) cannot be negative")
	checkParseSize(t, "42PeiB", 0, "Invalid size suffix: Got 'peib'")
	checkParseSize(t, "42PeB", 0, "Invalid size suffix: Got 'peb'")
	checkParseSize(t, "42ba", 0, "Invalid size suffix: Got 'ba'")
	checkParseSize(t, "42q", 0, "Invalid size suffix: Got 'q'")
}

// ----------------------------- Cmdline parsing ------------------------------

const (
	opt_name = `b2c.swap`
	swapfile = `/swapfile`
)

func checkExpectation(t *testing.T, parameters string, want SwapConfig) {
	opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: opt_name})

	got := ParseCmdline(opt, swapfile)
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Expected '%v', but got '%v'", want, got)
	}
}

func TestParseCmdline__no_opt(t *testing.T) {
	want := SwapConfig{Method: MethodNone}
	checkExpectation(t, ``, want)

	// Check that the defaults you get when passing nil as a command line match the empty command line
	got := ParseCmdline(nil, swapfile)
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Expected '%v', but got '%v'", want, got)
	}
}

func TestParseCmdline__with_block_device(t *testing.T) {
	// Mock getBlockDeviceMajorMinor to always return 42, 1337, nil... or panic if not called on /dev/sda like expected
	GetBlockDeviceMajorMinor = getBlockDeviceMajorMinorMock
	defer func() { GetBlockDeviceMajorMinor = getBlockDeviceMajorMinor }()

	checkExpectation(t, `b2c.swap=/dev/sda,resume`,
		SwapConfig{Method: MethodBlockDevice, SwapTarget: "/dev/sda", Size: 0, Swappiness: 60, Resume: true})
}

func TestParseCmdline__extra_args(t *testing.T) {
	checkExpectation(t, `b2c.swap=0,hello,world`, SwapConfig{Swappiness: 60})
}

func TestParseCmdline__with_size(t *testing.T) {
	checkExpectation(t, `b2c.swap=42MB`, SwapConfig{Method: MethodSwapFile, SwapTarget: swapfile,
		Size: 42000000, Swappiness: 60})
}

func TestParseCmdline__invalid_size(t *testing.T) {
	checkExpectation(t, `b2c.swap=-42MB`, SwapConfig{Size: 0, Swappiness: 60})
}

func TestParseCmdline__invalid_swappiness(t *testing.T) {
	checkExpectation(t, `b2c.swap=42MB,swappiness=201`,
		SwapConfig{Method: MethodSwapFile, SwapTarget: swapfile, Size: 42000000, Swappiness: 60})
}

func TestParseCmdline__swappiness(t *testing.T) {
	checkExpectation(t, `b2c.swap=42MB,swappiness=42`,
		SwapConfig{Method: MethodSwapFile, SwapTarget: swapfile, Size: 42000000, Swappiness: 42})
}

func TestParseCmdline__resume_on_swapfile_unsupported(t *testing.T) {
	checkExpectation(t, `b2c.swap=42MB,resume`,
		SwapConfig{Method: MethodSwapFile, SwapTarget: swapfile, Size: 42000000, Swappiness: 60, Resume: false})
}
