package runcmd

import (
	"fmt"
	"os"
	"os/exec"
	"testing"
)

func TestRunCallEquals(t *testing.T) {
	args := []string{"arg1", "arg2"}
	c := RunCall{Cmd: "cmd", Args: args}

	if err := c.Equals(c); err != nil {
		t.Fatalf("Comparing a RunCall to itself failed with the following error: %s\n", err.Error())
	}

	if err := c.Equals(RunCall{Cmd: "cmd_2", Args: args}); err != nil {
		err_want := "Mismatching calls!\nExpected: cmd_2 [\"arg1\" \"arg2\"]\nGot: cmd [\"arg1\" \"arg2\"]"
		if err.Error() != err_want {
			t.Fatalf("AssertEquals returned the wrong error message: Expected '%s' but got '%s'", err_want, err.Error())
		}
	}

	if err := c.Equals(RunCall{Cmd: "cmd", Args: []string{"arg1", "arg3"}}); err != nil {
		err_want := "Mismatching calls!\nExpected: cmd [\"arg1\" \"arg3\"]\nGot: cmd [\"arg1\" \"arg2\"]\nThe argument #1 differs: Expected 'arg3' but got 'arg2'"
		if err.Error() != err_want {
			t.Fatalf("AssertEquals returned the wrong error message: Expected '%s' but got '%s'", err_want, err.Error())
		}
	}
}

func TestRunCallAssertEquals(t *testing.T) {
	args := []string{"arg1", "arg2"}
	c := RunCall{Cmd: "cmd", Args: args}

	// Make sure we don't panic if equals
	c.AssertEquals(c)

	// Make sure we panic when we get different values
	defer func() {
		_ = recover()
	}()
	c.AssertEquals(RunCall{Cmd: "cmd_2", Args: args})
	t.Errorf("did not panic")
}

func ExecCommandMock(funcName string, command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=" + funcName, "--", command}
	cs = append(cs, args...)
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GOTEST_HELPER_PROCESS=1"}
	return cmd
}

func checkHelperTestArguments(success_exit_code int, want []string) {
	if os.Getenv("GOTEST_HELPER_PROCESS") != "1" {
		return
	}

	got := os.Args
	for len(got) > 0 {
		if got[0] == "--" {
			got = got[1:]
			break
		}
		got = got[1:]
	}

	if len(got) != len(want) {
		fmt.Printf("Argument count mismatch: Expected %q a but got %q\n", want, got)
	}

	for i, val := range got {
		if val != want[i] {
			fmt.Printf("\nExpected: %q\n     Got: %q\nThe argument #%d differs: Expected '%s' but got '%s'\n", want, got, i, val, want[i])
			os.Exit(success_exit_code + 1)
		}
	}

	os.Exit(success_exit_code)
}

func TestCallFail(t *testing.T) {
	checkHelperTestArguments(42, []string{"cmd", "arg1", "arg2"})
}

func fakeRunFail(command string, args ...string) *exec.Cmd {
	return ExecCommandMock("TestCallFail", command, args...)
}

func TestCallSuccess(t *testing.T) {
	checkHelperTestArguments(0, []string{"cmd", "arg1", "arg2"})
}

func fakeRunSuccess(command string, args ...string) *exec.Cmd {
	return ExecCommandMock("TestCallSuccess", command, args...)
}

func TestRun__ExpectedUseCase(t *testing.T) {
	execCommand = fakeRunFail
	if err := Run("cmd", []string{"arg1", "arg2"}); err != nil {
		err_want := "ERROR: The command \"cmd\" [arg1 arg2] failed: exit status 42"
		if err.Error() != err_want {
			t.Fatalf("Run returned the wrong error message: Expected '%s' but got '%s'", err_want, err.Error())
		}
	} else {
		t.Fatalf("Expected Run to return an error")
	}

	execCommand = fakeRunSuccess
	if err := Run("cmd", []string{"arg1", "arg2"}); err != nil {
		t.Fatalf("Did not expect Run to return an error. Got '%s'\n", err.Error())
	}
}

func TestRun__MockingUseCase(t *testing.T) {
	CallsHistory = make([]RunCall, 0)

	MockCallback = func(cmdName string, args []string) error {
		if cmdName == "cmd" {
			return Success(cmdName, args)
		} else {
			return Fail(cmdName, args)
		}
	}

	args := []string{"arg1", "arg2"}

	if err := Run("cmd_fail", args); err != nil {
		err_want := MockExecutionError.Error()
		if err.Error() != err_want {
			t.Fatalf("Run returned the wrong error message: Expected '%s' but got '%s'", err_want, err.Error())
		}
	} else {
		t.Fatalf("Expected Run to return an error")
	}

	if err := Run("cmd", []string{"arg1", "arg2"}); err != nil {
		t.Fatalf("Did not expect Run to return an error. Got '%s'\n", err.Error())
	}

	want := 2
	if len(CallsHistory) != want {
		t.Fatalf("Got an unexpected amount of calls in our Run call history: Wanted %d, but got %d\n", want, len(CallsHistory))
	}

	CallsHistory[0].AssertEquals(RunCall{Cmd: "cmd_fail", Args: args})
	CallsHistory[1].AssertEquals(RunCall{Cmd: "cmd", Args: args})
}
