package runcmd

import (
	"fmt"
	"os"
	"os/exec"
)

type RunMockCallback func(cmdName string, args []string) error

type RunCall struct {
	Cmd  string
	Args []string
}

func (c RunCall) Equals(o RunCall) error {
	err := fmt.Sprintf("Mismatching calls!\nExpected: %s %q\nGot: %s %q", o.Cmd, o.Args, c.Cmd, c.Args)

	if c.Cmd != o.Cmd || len(c.Args) != len(o.Args) {
		return fmt.Errorf(err)
	}

	for i, val := range c.Args {
		if val != o.Args[i] {
			err = fmt.Sprintf("%s\nThe argument #%d differs: Expected '%s' but got '%s'", err, i, o.Args[i], val)
			return fmt.Errorf(err)
		}
	}

	return nil
}

func (c RunCall) AssertEquals(o RunCall) {
	if err := c.Equals(o); err != nil {
		panic(err)
	}
}

var (
	// Allow tests to override the following functions to simplify testing
	execCommand = exec.Command

	// Calls history, to be inspected by tests
	CallsHistory []RunCall = nil

	// Mock the entire call
	MockCallback RunMockCallback = nil

	MockExecutionError = fmt.Errorf("Mock execution error")
)

func Success(cmdName string, args []string) error {
	return nil
}

func Fail(cmdName string, args []string) error {
	return MockExecutionError
}

func Run(cmdName string, args []string) error {
	if CallsHistory != nil {
		CallsHistory = append(CallsHistory, RunCall{Cmd: cmdName, Args: args})
	}

	if MockCallback != nil {
		return MockCallback(cmdName, args)
	} else {
		cmd := execCommand(cmdName, args...)

		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			return fmt.Errorf("ERROR: The command %q %s failed: %s", cmdName, args, err.Error())
		}

		return nil
	}
}
