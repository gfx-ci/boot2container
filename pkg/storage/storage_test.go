package storage

import (
	"bytes"
	"reflect"
	"testing"

	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/cmdline"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/filesystem"
	"gitlab.freedesktop.org/gfx-ci/boot2container/pkg/runcmd"
)

const (
	MiB = 1024 * 1024
)

var (
	DefaultCfg = StorageConfig{Driver: "overlay",
		TransientStore: false,
		PullOptions: map[string]bool{
			"enable_partial_images": true,
			"use_hard_links":        true,
			"convert_images":        false},
		OverlayMountOpt:     []string{"nodev"},
		OverlayUseComposeFS: false,
	}
)

func assertEqual(t *testing.T, field string, a interface{}, b interface{}) {
	if a != b {
		t.Fatalf("%s: `%v` != `%v`", field, a, b)
	}
}

// --------------------------------- Generate ---------------------------------

func TestGenerate__parsing_failure(t *testing.T) {
	tmplStr := `Driver: {{.Driver`
	assertEqual(t, "Generate() return value", DefaultCfg.Generate(tmplStr, nil).Error(),
		"Failed to parse the template: template: StorageCfg:1: unclosed action")
}

func TestGenerate__exec_failure(t *testing.T) {
	tmplStr := `{{ index .OverlayMountOpt 1 }}`
	out := new(bytes.Buffer)

	assertEqual(t, "Generate() return value", DefaultCfg.Generate(tmplStr, out).Error(),
		`Failed to execute the template: template: StorageCfg:1:3: executing "StorageCfg" at <index .OverlayMountOpt 1>: error calling index: reflect: slice index out of range`)
}

func TestGenerate__success(t *testing.T) {
	cfg := StorageConfig{Driver: "overlay",
		AdditionalImageStores: []filesystem.FilesystemConfig{
			filesystem.FilesystemConfig{Name: "MyFS", Src: "/dev/mockblk", Type: "", Opts: []string{}},
		},
		PullOptions: map[string]bool{
			"enable_partial_images": true,
			"use_hard_links":        true,
			"convert_images":        false},
		OverlayMountOpt:     []string{"nodev"},
		OverlayUseComposeFS: false,
	}

	tmplStr := `Driver: {{.Driver}}
PullOptions: {{.PullOptions}}
OverlayMountOpt: {{.OverlayMountOpt}}
OverlayUseComposeFS: {{.OverlayUseComposeFS}}
`
	// Capture the Mount calls
	runcmd.CallsHistory = make([]runcmd.RunCall, 0)
	runcmd.MockCallback = runcmd.Fail

	// Generate the output, stored in a bytes Buffer
	out := new(bytes.Buffer)
	assertEqual(t, "Generate() return value", cfg.Generate(tmplStr, out), nil)

	// Check the generated output
	want := `Driver: overlay
PullOptions: map[convert_images:false enable_partial_images:true use_hard_links:true]
OverlayMountOpt: [nodev]
OverlayUseComposeFS: false
`
	assertEqual(t, "Generated template", out.String(), want)

	// Make sure the we tried mounting the filesystem
	if len(runcmd.CallsHistory) != 1 {
		t.Fatalf("We expected 1 call, got %q\n", runcmd.CallsHistory)
	}
	expected := runcmd.RunCall{"mount", []string{"/dev/mockblk", "/mnt/fs_MyFS"}}
	runcmd.CallsHistory[0].AssertEquals(expected)
}

// ----------------------------- Cmdline parsing ------------------------------

const (
	opt_name = `b2c.storage`
)

func checkExpectation(t *testing.T, parameters string, want StorageConfig) {
	opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: opt_name})

	// Preload the filesystems
	fs_opt := cmdline.FindOptionInString(parameters, cmdline.OptionQuery{Name: "b2c.filesystem"})
	fses := filesystem.ListCmdlineFilesystems(fs_opt)

	got := ParseCmdline(opt, fses)
	if !reflect.DeepEqual(got, want) {
		t.Fatalf("Expected:\n\t'%#v'\nbut got\n\t'%#v'", want, got)
	}
}

func TestParseCmdline__no_opt(t *testing.T) {
	checkExpectation(t, ``, DefaultCfg)

	// Check that the defaults you get when passing nil as a command line match the empty command line
	got := ParseCmdline(nil, nil)
	if !reflect.DeepEqual(got, DefaultCfg) {
		t.Fatalf("Expected '%#v', but got '%#v'", DefaultCfg, got)
	}
}

func TestParseCmdline__empty_value(t *testing.T) {
	want := StorageConfig{Driver: "overlay",
		TransientStore: false,
		PullOptions: map[string]bool{
			"convert_images":        false,
			"enable_partial_images": false,
			"use_hard_links":        false},
		OverlayMountOpt:     []string{},
		OverlayUseComposeFS: false,
	}
	checkExpectation(t, `b2c.storage=""`, want)
}

func TestParseCmdline__invalid_opt_values(t *testing.T) {
	want := StorageConfig{Driver: "overlay",
		TransientStore: false,
		PullOptions: map[string]bool{
			"convert_images":        false,
			"enable_partial_images": false,
			"use_hard_links":        false},
		OverlayMountOpt:     []string{},
		OverlayUseComposeFS: false,
	}
	checkExpectation(t, `b2c.storage="invalidarg,transient_store=invalid,overlay.use_composefs=invalid,pull_options=unknown"`, want)
}

func TestParseCmdline__all_options_set(t *testing.T) {
	want := StorageConfig{Driver: "btrfs",
		TransientStore: true,
		AdditionalImageStores: []filesystem.FilesystemConfig{
			filesystem.FilesystemConfig{Name: "fs1", Src: "/dev/sda1", Type: "", Opts: []string{}},
			filesystem.FilesystemConfig{Name: "fs2", Src: "/dev/sda2", Type: "", Opts: []string{}},
		},
		PullOptions: map[string]bool{
			"convert_images":        true,
			"enable_partial_images": true,
			"use_hard_links":        true},
		OverlayMountOpt:     []string{"nodev", "ro"},
		OverlayUseComposeFS: true,
	}
	checkExpectation(t, `b2c.filesystem=fs1,src=/dev/sda1 b2c.filesystem=fs2,src=/dev/sda2 b2c.storage="driver=btrfs,transient_store=true,pull_options=convert_images|enable_partial_images|use_hard_links,additionalimagestores=fs1|fsinvalid|fs2,overlay.mountopt=nodev|ro,overlay.use_composefs=true"`, want)
}
